<div class="row">
    <div style="background-color: white; border: 1px solid #e0d8d8;margin-bottom: 5px;">
        <div class="box-body">
            <div class="error-page">
                <h2 class="headline text-red">500</h2>
                <div class="error-content">
                    <h3><i class="fa fa-warning text-red"></i> Oops! You have not permission.</h3>
                    <p>
                        We will work on fixing that right away.
                        you may <a href="index.php">return</a>.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>