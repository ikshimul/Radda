$(function () {
    $("#example1").DataTable({
        "ordering": false,
        "pageLength": 50
    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
    });
});